const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const pkg = require('./package.json');

module.exports = ({ mode }) => {
  const plugins = [
    new webpack.ProgressPlugin(),
    new webpack.DefinePlugin({
      PRODUCTION: mode === 'production',
      VERSION: pkg.version,
    }),
    new CleanWebpackPlugin()
  ];
  if (mode === 'development')
    plugins.push(
      new HtmlWebpackPlugin({
	filename: 'index.html',
	template: path.resolve(__dirname, 'public/index.html')
      }),
      new CopyPlugin([
	{
          from: path.resolve(__dirname, 'fixtures'),
          to: path.resolve(__dirname, 'dist'),
	}
      ])
    );
  return {
    mode: mode,
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
      path: path.resolve(__dirname, mode === 'development' ? 'dist' : 'lib'),
      filename: 'index.js',
      library: 'pageXML'
    },
    module: {
      rules: [
	{
	  test: /\.m?js$/,
	  exclude: /(node_modules|bower_components)/,
	  use: {
            loader: 'babel-loader'
	  }
	},
	{
          test: /\.js$/,
          use: ['source-map-loader'],
          enforce: 'pre',
	}
      ]
    },
    plugins: plugins,
    resolve: {
      extensions: ['.js'],
      modules: [
	path.resolve(__dirname, 'src'),
	path.resolve(__dirname, 'node_modules')
      ],
      alias: {
	'~': path.resolve(__dirname, 'src'),
      },
    },
    devtool: mode === 'development' ? 'cheap-module-eval-source-map' : 'hidden-source-map',
    devServer: {
      contentBase: path.resolve(__dirname, 'dist'),
    }
  };
};
