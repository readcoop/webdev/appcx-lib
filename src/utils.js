export function fromPointsAttribute(string) {
  return string.split(' ').map(function(string) {
    const [x, y] = string.split(',').map(Number);
    return { x, y };
  });
}

function pointToString(p) {
  return p.x + ',' + p.y;
}

function pointToArray(point) {
  return point.toArray();
}

export function toPointsAttribute(points) {
  return points.map((point) => pointToString(point)).join(' ');
}

export function fromCustomAttribute(string) {
  console.assert(typeof string === 'string');

  if (string === '')
    return [];

  return string.split('} ').reduce(function(results, string) {
    const result = {};
    const attributes = {};

    const name = string.split(' {')[0];
    let attrs = string.split(' {')[1];

    if (attrs.indexOf('}'))
      attrs = attrs.replace('}', '');

    let key, value;

    return results.concat({
      name: name.trim(),
      attributes: attrs.split(';').reduce(function(attrs, string) {
        string = string.trim();
        if (string) {
          key = string.split(':')[0].trim();
          value = decodeURIComponent(JSON.parse('"' + string.split(":")[1].trim().replace('"', '\\"') + '"'));
          if (key === 'index' || key === 'offset' || key === 'length')
            attrs[key] = parseInt(value);
          else if (key === 'continued' || key === 'superscript' || key === 'bold' || key === 'italic' || key === 'subscript')
            attrs[key] = Boolean(value);
          else
            attrs[key] = value;
        }
        return attrs;
      }, {}),
    });
  }, []);
}

export function toCustomAttributeString(attrList) {
  return attrList.reduce(function(string, item) {
    string += item.name + ' {';
    for (const key in item.attributes) {
      let value;
      switch (typeof item.attributes[key]) {
        case 'number':
        case 'boolean':
          value = item.attributes[key].toString();
          break;
        default:
          value = toUnicode(item.attributes[key]);
      }
      string += key + ':' + value + '; ';
    }
    return string.trim() + '} ';
  }, '').trim();
}

function toUnicode(string) {
  let result = '';
  for (let i = 0; i < string.length; i++) {
    // NOTE: assumes all characters are < 0xffff
    if ( /^[a-zA-Z0-9]*$/.test(string[i]) )
      result += string[i];
    else
      result += '\\u' + ('000' + string[i].charCodeAt(0).toString(16)).substr(-4);
  }
  return result;
}

function parseReadingOrder(customAttrValue) {
  return findReadingOrder(parseCustomAttribute(customAttrValue));
}

function findReadingOrder(attrList) {
  for (const entry of attrList) {
    if (entry.name === 'readingOrder')
      return entry.attributes.index;
  }
  return null;
}
