import { xml2js, js2xml } from 'xml-js';
import * as jp from 'jsonpath';

import {
  fromPointsAttribute,
  fromCustomAttribute,
  toPointsAttribute,
  toCustomAttribute
} from './utils.js';

export function from(string, options = { }) {
  const handlers = {
    points: (...args) => fromPointsAttribute(...args),
    custom: (...args) => fromCustomAttribute(...args)
  };

  const data = xml2js(string, {
    compact: false,
    alwaysArray: true,
    alwaysChildren: true,
    nativeType: true,
    nativeTypeAttributes: true,
    elementsKey: 'children',
    attributeValueFn: (value, name, parent) => name in handlers
      ? handlers[name](value)
      : value
  });

  const { what = 'default' } = options;

  if (what === 'lines') {
    return extractLines(data).map(
      (line) => {
	const { id, ...attributes } = line.attributes;
	return {
	  id,
	  attributes,
	  text: extractUnicode(line)
	    .map((unicode) => extractTextFromUnicode(unicode)),
	  baseline: extractPointsFromBaseline(extractBaseline(line)),
	  coords: extractPointsFromCoords(extractCoords(line))
	};
      }
    );
  }
  else if (what === 'regions' || what == 'regions+lines') {
    return extractRegions(data).map(
      (region) => extractLines(region).map(
	(line) => extractUnicode(line).map(
	  (unicode) => extractTextFromUnicode(unicode)
	)
      )
    );
  }
  else if (what === 'regions+lines+attributes') {
    return extractRegions(data).map(
      (region) => {
        if(region.name === 'TableRegion'){
          console.log('extractRegion')
          console.log(region)
          const { id, ...attributes } = region.attributes;
        return {
          id,
          attributes,
          type: region.name,
          coords: extractPointsFromCoords(extractCoords(region)),
          children : extractTableCells(region).map(
            (cell) => {
              const { id, ...attributes } = cell.attributes;
              return {
          id,
          children : extractLines(cell).map(
            (line) => {
              const { id, ...attributes } = line.attributes;
              return {
          id,
          attributes: line.attributes,
          text: extractUnicode(line).map(
            (unicode) => extractTextFromUnicode(unicode)
          ),
          coords: extractPointsFromCoords(extractCoords(line))
              };
            }
          ),
          attributes: cell.attributes,
              };
            }
          ),
        };
        }
        
  const { id, ...attributes } = region.attributes;
	return {
	  id,
    attributes,
    type: region.name,
	  coords: extractPointsFromCoords(extractCoords(region)),
	  children: extractLines(region).map(
	    (line) => {
	      const { id, ...attributes } = line.attributes;
	      return {
		id,
		attributes: line.attributes,
		text: extractUnicode(line).map(
		  (unicode) => extractTextFromUnicode(unicode)
		),
		coords: extractPointsFromCoords(extractCoords(line))
	      };
	    }
	  )
	};
      }
    );
  }
  console.log(data)
  return data;
}

function extractRegions(data) {
  const typeExpr = [
    'TextRegion',
    'ImageRegion',
    'GraphicRegion',
    'ChartRegion',
    'LineDrawingRegion',
    'SeparatorRegion',
    'TableRegion',
    'MathsRegion',
    'ChemRegion',
    'MusicRegion',
    'AdvertRegion',
    'NoiseRegion',
    'UnknownRegion'
  ].map((regionType) => `@.name=="${regionType}"`).join(' || ');
  console.log(typeExpr)
  const expr = `$..children[?(${typeExpr})]`;

  return jp.query(data, expr);
}

function extractTableCells(data) {
  const expr = `$..children[?(@.name=="TableCell")]`;
  return jp.query(data, expr);
}

function extractLines(data) {
  const expr = `$..children[?(@.name=="TextLine")]`;
  return jp.query(data, expr);
}

function extractCoords(data) {
  const expr = `$..children[?(@.name=="Coords")]`;
  return jp.query(data, expr).shift() || [];
}

function extractBaseline(data) {
  const expr = `$..children[?(@.name=="Baseline")]`;
  return jp.query(data, expr).shift() || [];
}

function extractUnicode(data) {
  const expr = `$..children[?(@.name=="TextEquiv")]..children[?(@.name=="Unicode")]`;
  return jp.query(data, expr);
}

function extractPointsFromCoords(data) {
  const { attributes = {} } = data;
  return attributes.points || [];
}

function extractPointsFromBaseline(data) {
  const { attributes = {} } = data;
  return attributes.points || [];
}

function extractTextFromUnicode(data) {
  const expr = `$..children[?(@.type=="text")]`;
  return jp
    .query(data, expr)
    .map((item) => item.text)
    .join('');
}

export function to(data) {
  throw new Error("Not Implemented");
}
